var expect = require('chai').expect;
var { Builder, By, Key, until } = require('selenium-webdriver');
var notifier = require('./notifier');


describe("Email varification", () => {
  const URL ="http://68.183.57.185:8002/"
  const validEmail = 'neginmashreghi.nm@outlook.com'

  const driver = new Builder()
   
    /* .usingServer("http://localhost:4444/wd/hub")
    // Setting capabilities when requesting a new session
    .withCapabilities({"browserName":"chrome"})
    // Creates a new WebDriver client based on this builder's current configuration. 
    .build(); */
 
    .forBrowser('chrome')
    .build();


  it('check Verify endpoint with correct pin', async () => {
    // laod "http://68.183.57.185:8002/"
    await driver.get(URL);
    await driver.findElement(By.id('to')).sendKeys(validEmail, Key.ENTER);
    await driver.findElement(By.id('send_email')).click();
    
    // wiating for a promise that will be resolve after the email appier to the user's Inbox
    // resolve return mail json that contains all infomation about new email
    const mail = await notifier.getNewMail();
    //console.log(mail , typeof mail)

    
    //Executes a search on a string using a regular expression pattern, and returns an array containing the results of that search.
    const pin_rx = /<!--pin-->(.*?)<!--pin-->/.exec(mail.html)
    //console.log(pin_rx , typeof pin_rx)
   
    await driver.findElement(By.id('pin')).sendKeys(pin_rx[1], Key.ENTER);
    await driver.findElement(By.id('varified_code')).click();
    
    // wait until an html elemet with 'message' id conatine spesific innertext
    const msg = "Your email has been varified"
    await driver.wait(until.elementTextIs(await driver.findElement(By.id('message')), msg), 7 * 10000);
  });   
  after(async () => driver.quit());

});








