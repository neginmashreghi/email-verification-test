### Description
Automated testing a email verification application with Selenium WebDriver and Node.js

---

### Requirements
clone and Run the Email verification application before running test.js

```
git clone https://neginmash@bitbucket.org/neginmash/2fa-emailverification.git

```
---

### - test.js 
test.js file contain functional test using [mocha](https://mochajs.org/) test framework and [selenium-webdriver](https://www.npmjs.com/package/selenium-webdriver) browser automation library.

---

### - Bitbuket-pipeline.yml 
Bitbucket Pipelines is an integrated CI/CD service, built into Bitbucket. It allows you to automatically test based on a Bitbuket-pipeline.yml in your repository.  

--- 

### - notifier.js
notifier.js file contains a [mail-notifier](https://www.npmjs.com/package/mail-notifier) library that sends mail event for each new email in IMAP INBOX
